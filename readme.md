## Automation Notes

1. For reasons unknown the `reconfigure` step of `gitswarm` fails on AWS via `OpsWorks`.  The logs report a `bind` failure due to a port already being in use.  Interestingly, when shelling into the instance following a `chef` run the reconfigure works as expected.  So, in order to get going
the `reconfigure` step has been disabled when deploying to AWS (via custom json).  Following a deployment please shell into the fresh instance and as `root` run:

	gitswarm-ctl reconfigure


