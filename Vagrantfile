# -*- mode: ruby -*-
# vi: set ft=ruby :

# ONGOING: add new apps/services that this repo is responsible for to this run_lists hash
run_lists = {
	'swarm' => [
		'recipe[ops-infrastructure::gitswarm]',
		'recipe[ops-infrastructure::gitswarm-configure]',
	],
	'fushion' => [
	]
}

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.require_version ">= 1.5.0"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
	# All Vagrant configuration is done here. The most common configuration
	# options are documented and commented below. For a complete reference,
	# please see the online documentation at vagrantup.com.

	# Every Vagrant virtual environment requires a box to build off of.
	# If this value is a shorthand to a box in Vagrant Cloud then 
	# config.vm.box_url doesn't need to be specified.
	config.vm.box = "bento/ubuntu-14.04"
	config.vm.hostname = "dev-infrastrucure-berkshelf"

	# Provider-specific configuration so you can fine-tune various
	# backing providers for Vagrant. These expose provider-specific options.
	config.vm.provider :virtualbox do |vb|
		vb.destroy_unused_network_interfaces = true
	  vb.memory = 1024
	  vb.cpus = 1
	end
	
	# Set the version of chef to install using the vagrant-omnibus plugin
	config.omnibus.chef_version = '11.10'

	# Use Berkshelf to manage recipe dependencies
	config.berkshelf.enabled = true

	# Enable provisioning with chef solo, specifying a cookbooks path, roles
	# path, and data_bags path (all relative to this Vagrantfile), and adding
	# some recipes and/or roles.
	config.vm.provision :chef_solo do |chef|
    action = ARGV[0]
    if action == 'up' or action == 'provision' then
      app = ENV['APP']
      if not run_lists.has_key?(app) then
        puts("\nNo valid app defined. Provided value was '#{app}'.")
        puts("Try APP=<app_name> vagrant up")
        puts("Where <app_name> is one of [#{run_lists.keys.join(', ')}].")
        puts("")
        puts("If your app is valid, but not present in the list, add it's run list to the Vagrantfile.\n")
        puts("Defaulting to 'gitswarm'...")
        app = 'swarm'
      end
    end

		chef.run_list = run_lists[app]

		chef.json = {
		}
	end
	
	# Create a forwarded port mapping which allows access to a specific port
	# within the machine from a port on the host machine. In the example below,
	# accessing "localhost:8080" will access port 80 on the guest machine.
	# config.vm.network "forwarded_port", guest: 5672, host: 5672
	# config.vm.network "forwarded_port", guest: 15672, host: 15672
	# config.vm.network "forwarded_port", guest: 443, host: 8080

	# The url from where the 'config.vm.box' box will be fetched if it
	# is not a Vagrant Cloud box and if it doesn't already exist on the 
	# user's system.
	# config.vm.box_url = "https://vagrantcloud.com/chef/ubuntu-14.04/version/1/provider/virtualbox.box"

	# Assign this VM to a host-only network IP, allowing you to access it
	# via the IP. Host-only networks can talk to the host machine as well as
	# any other machines on the same network, but cannot be accessed (through this
	# network interface) by any external networks.
	config.vm.network :public_network, type: "dhcp"

	# Share an additional folder to the guest VM. The first argument is
	# the path on the host to the actual folder. The second argument is
	# the path on the guest to mount the folder. And the optional third
	# argument is a set of non-required options.
	# config.vm.synced_folder "../data", "/vagrant_data"

	# The path to the Berksfile to use with Vagrant Berkshelf
	# config.berkshelf.berksfile_path = "./Berksfile"

	# An array of symbols representing groups of cookbook described in the Vagrantfile
	# to exclusively install and copy to Vagrant's shelf.
	# config.berkshelf.only = []

	# An array of symbols representing groups of cookbook described in the Vagrantfile
	# to skip installing and copying to Vagrant's shelf.
	# config.berkshelf.except = []
end
