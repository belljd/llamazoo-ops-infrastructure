default[:postfix][:use_ses] = true

default[:postfix][:mail_type] = 'master'

default[:postfix][:main][:relayhost] = '[email-smtp.us-west-2.amazonaws.com]:587'
default[:postfix][:main][:myhostname] = 'git.llamazoo.com'
default[:postfix][:main][:mydomain] = 'git.llamazoo.com'
default[:postfix][:main][:inet_interfaces] = 'loopback-only'
default[:postfix][:main][:smtp_use_tls] = 'yes'
default[:postfix][:main][:smtp_tls_CAfile] = '/etc/ssl/certs/ca-certificates.crt'
default[:postfix][:main][:smtpd_tls_CAfile] = '/etc/ssl/certs/ca-certificates.crt'
default[:postfix][:main][:smtp_tls_security_level] = 'encrypt'
default[:postfix][:main][:smtp_sasl_auth_enable] = 'yes'

default[:postfix][:sasl][:smtp_sasl_user_name] = 'this should not be in source control'
default[:postfix][:sasl][:smtp_sasl_passwd] = 'this should not be in source control'
