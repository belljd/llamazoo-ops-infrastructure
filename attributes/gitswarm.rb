default[:gitswarm][:distro] = 'trusty'

default[:gitswarm][:fqdn] = 'https://git.llamazoo.com'
default[:gitswarm][:http_redirect] = true

default[:gitswarm][:time_zone] = 'Pacific Time (US & Canada)'
default[:gitswarm][:email][:from] = 'info@llamazoo.com'
default[:gitswarm][:email][:display_name] = 'GitSwarm'
default[:gitswarm][:email][:reply_to] = 'info@llamazoo.com'

default[:gitswarm][:issues] = false
default[:gitswarm][:merge_requests] = true
default[:gitswarm][:wiki] = true
default[:gitswarm][:snippets] = true
default[:gitswarm][:ci] = true

default[:gitswarm][:ssl][:directory] = '/etc/gitswarm/ssl'
default[:gitswarm][:ssl][:key_file] = '/etc/gitswarm/ssl/git.llamazoo.com.key'
default[:gitswarm][:ssl][:key_file_data] = 'this should not be in source control'
default[:gitswarm][:ssl][:cert_file] = '/etc/gitswarm/ssl/git.llamazoo.com.crt'
default[:gitswarm][:ssl][:key_file_data] = 'this should not be in source control'
default[:gitswarm][:ssl][:chain_file] = '/etc/gitswarm/ssl/git.llamazoo.com.chain'
default[:gitswarm][:ssl][:chain_file_data] = 'this should not be in source control'

default[:gitswarm][:aws_backup] = true
default[:gitswarm][:s3][:region] = 'us-west-2'
default[:gitswarm][:s3][:key_id] = 'this should not be in source control'
default[:gitswarm][:s3][:access_key] = 'this should not be in source control'
default[:gitswarm][:s3][:bucket_name] = 'gitswarm-backup'
default[:gitswarm][:s3][:ci][:bucket_name] = 'gitswarm-ci-backup'

default[:gitswarm][:use_fushion] = false

default[:gitswarm][:aws] = false
