#
# Cookbook Name:: ops-infrastructure
# Recipe:: gitswarm-configure
#
# Configure Perforce GitSwarm for use.
#

# write the settings file
template '/etc/gitswarm/gitswarm.rb' do
	source 'gitswarm.rb.erb'
	action :create
end

directory node[:gitswarm][:ssl][:directory] do
	action :create
end

# write the SSL private key
key_data = StringIO.new
key_data.puts(node[:gitswarm][:ssl][:key_file_data])

file node[:gitswarm][:ssl][:key_file] do
	content key_data.string
	action :create
end

# write the SSL cert
cert_data = StringIO.new
cert_data.puts(node[:gitswarm][:ssl][:cert_file_data])

file node[:gitswarm][:ssl][:cert_file] do
	content cert_data.string
	action :create
end

# write the signing bundle
chain_data = StringIO.new
chain_data.puts(node[:gitswarm][:ssl][:chain_file_data])

file node[:gitswarm][:ssl][:chain_file] do
	content chain_data.string
	action :create
end

# restart the services
if node[:gitswarm][:aws] == false
	bash "reconfigure the gitswarm services" do
		user "root"
		code "gitswarm-ctl reconfigure"
		timeout 3600
	end
end
