#
# Cookbook Name:: ops-infrastructure
# Recipe:: gitswarm
#
# Install the Perforce GitSwarm service.
#

package "software-properties-common" do
	action :install
end

case node[:platform]
	when 'ubuntu','debian'
		bash "add BrightBox repositories" do
			user "root"
			code "apt-add-repository ppa:brightbox/ruby-ng"
		end

		bash "install Perforce's packaging key" do
			user "root"
			code "wget -q https://package.perforce.com/perforce.pubkey -O - | apt-key add -"
		end

		bash "add Perforce's packages to our list" do
			user "root"
			code "sh -c \"echo deb http://package.perforce.com/apt/ubuntu #{node[:gitswarm][:distro]} release > /etc/apt/sources.list.d/perforce.list\""
		end

		bash "update package repositories" do
			user "root"
			code "apt-get -y update"
		end

		bash "upgrade installed packages" do
			user "root"
			code "apt-get -y upgrade"
		end
				
	when 'centos','redhat','fedora','amazon'
		raise('Only support "debian" and "ubuntu" instances at this time')
end

include_recipe 'postfix'

ruby_block "append SMTP postfix config" do
	block do
		file = Chef::Util::FileEdit.new('/etc/postfix/main.cf')
		file.insert_line_if_no_match('/smtp_tls_note_starttls_offer/','smtp_tls_note_starttls_offer = yes')
		file.write_file()
	end
	notifies :restart,'service[postfix]',:delayed
end

if node[:postfix][:use_ses] == true
	ruby_block "update master postfix config" do
		block do
			file = Chef::Util::FileEdit.new('/etc/postfix/master.cf')
			file.search_file_replace_line('/-o smtp_fallback_relay=/','#-o smtp_fallback_relay=')
			file.write_file()
		end
		notifies :restart,'service[postfix]',:delayed
	end
end

package 'curl' do
	action :install
end

package 'ruby2.3' do
	action :install
end

case node[:platform]
	when 'ubuntu','debian'
		timeout 3600 do
			package 'helix-gitswarm' do
				action :install
			end
		end

  when 'centos','redhat','fedora','amazon'
    raise('Only support "debian" and "ubuntu" instances at this time')
end
