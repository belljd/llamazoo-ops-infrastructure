name             'ops-infrastructure'
maintainer       'Llama Zoo Interactive'
maintainer_email 'dave@llamazoo.com'
license          'All rights reserved'
description      'Installs/Configures the cloud systems used by the development team.'
long_description IO.read(File.join(File.dirname(__FILE__), 'readme.md'))
version          '0.1.0'

depends 'postfix'
